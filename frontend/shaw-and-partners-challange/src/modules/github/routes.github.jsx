import Users from "./views/users/Users";

const routes = [
    {
        module: '/github',
        path: '/users',
        icon: 'fab fa-github',
        component: Users,
        label: 'Users'
    }
]

export default routes