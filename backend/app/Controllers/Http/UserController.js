'use strict'

const GithubHttpsClient = use('App/Clients/GithubHttpsClient')

class UserController {

    /**
     * List paging users from github 
     */
    async index ({request }) {
        const { since } = request.all()

        let url = '/users'
        if (since)
            url += `?since=${since}`

        const client = new GithubHttpsClient()
        const users = await client.get(url)
        return users
    }

     /**
     * Show details from user by user_name
     */
    async details ({request, params }) {
        const { user_name } = params

        let url = '/users'
        if (user_name)
            url += `/${user_name}`

        const client = new GithubHttpsClient()
        const user = await client.get(url)
        return user
    }

    /**
     * List user repos from user by user_name
     */
    async repos ({request, params }) {
        const { user_name } = params

        let url = '/users'
        if (user_name)
            url += `/${user_name}/repos`

        const client = new GithubHttpsClient()
        const user = await client.get(url)
        return user
    }

}

module.exports = UserController
