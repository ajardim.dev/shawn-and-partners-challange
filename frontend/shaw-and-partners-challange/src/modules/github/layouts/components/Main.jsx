import React from 'react';
import { Route, Switch} from "react-router-dom"

export default function Main(props) {
    const {routes} = props

    return <main className="col bg-faded py-3 flex-grow-1">
        <Switch>
            {routes.map((route,i) => 
                <Route key={i} path={`${route.module}${route.path}`} render={route.component    } />
            )}
        </Switch>
    </main>
}
