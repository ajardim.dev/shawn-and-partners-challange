'use strict'

const https = use('https')
const Env = use('Env')

class GithubHttpsClient {

    get(path) { 
        return new Promise ((resolve, reject) => { 
            const requestOptions = {
                host: Env.get('GIT_BASE_URL', ''),
                method: 'GET',
                headers: {
                    'user-agent': 'node.js',
                    'Authorization': Env.get('GIT_TOKEN', '')
                },
                path
            }
            
            https.request(requestOptions, resp => {
                let data = ''
                resp.on("data", chunk => data += chunk)

                resp.on("end", () => {
                    resolve(data)
                })

                resp.on('error', resolve)
            })
            .on('error', resolve)
            .end()
        })
    }
}

module.exports = GithubHttpsClient