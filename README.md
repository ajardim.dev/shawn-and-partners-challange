![](https://shawandpartners.com/wp-content/uploads/2017/02/LOGO_FINAL_SP.png)
# Arthur Jardim (Full-stack Developer) - +55 (51) 9 8555-0717


[Hi, this is my challenge execution. I made it with <3  and I hope you enjoy!](http://arthur-shaw-partners-challange.herokuapp.com/github/users)
 
 ![](https://powerdoctorblob.blob.core.windows.net/test/Screenshot_1.png)
 
 

## Backend: Why Adonis Js

For this application, I chose AdonisJS because it is very productive and simple and is a framework that brings an architectural opinion and helps a lot when you have programmers with different skill levels and working remotely.

This comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

#### Get Started

```bash
yarn install
adonis migration:run
adonis serve --dev
```
#### Run Tests

```bash
adonis test
```

## Frontend: Why Reactjs


I chose reacjs because of the very active community and the constant evolution of the framework for productivity. I didn't choose to use typescript for more productivity using redux and react hooks.

This app have:
1. Redux
2. Duck Pattern
2. React Hooks
3. React Redux
2. Bootstrap Grid
2. Material Ui
3. Infinite scroll

#### Get Started

```bash
yarn install
yarn start
```


