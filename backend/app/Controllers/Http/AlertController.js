'use strict'

const Alert = use("App/Models/Alert")

class AlertController {
  /**
   * Show a list of all alerts.
   * GET alerts
   *
   */
  async index () {
    const alerts = await Alert
      .query()
      .with("user")
      .fetch()

    return alerts
  }
  
  /**
   * Create/save a new alert.
   * POST alerts
   *
   */
  async store ({ request, auth }) {
    const data = request.only(['content', 'route'])
    const alert = await Alert.create({user_id: auth.user.id, ...data})

    return alert
  }

  /**
   * Display a single alert.
   * GET alerts/:id
   *
   */
  async show ({ params }) {
    const alert = await Alert.findOrFail(params.id)
    await Promise.all([
      alert.load('user'),
    ])

    return alert
  }

  /**
   * Delete a alert with id.
   * DELETE alerts/:id
   *
   */
  async destroy ({ params, auth, response }) {
    const alert = await Alert.findOrFail(params.id)

    if(alert.user_id !== auth.user.id) {
      return response.status(401)
    }

    await alert.delete()
  }
}

module.exports = AlertController
