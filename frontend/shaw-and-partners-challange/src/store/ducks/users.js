import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

export const { Types, Creators } = createActions({
    userRepos:['userRepos'],
    showUser:['user'],
    addUsers:['users'],
});

export const INITIAL_STATE = Immutable({ 
    users:[],
    user:null,
    userRepos: []
});

export const addUsers = (state = INITIAL_STATE, action) => ({...state, users: state.users.concat(action.users) })
export const showUser = (state = INITIAL_STATE, action) => ({...state, user: action.user })
export const userRepos = (state = INITIAL_STATE, action) => ({...state, userRepos: action.userRepos })

export default createReducer(INITIAL_STATE, {
    [Types.ADD_USERS]: addUsers,
    [Types.SHOW_USER]: showUser,
    [Types.USER_REPOS]: userRepos,
});
