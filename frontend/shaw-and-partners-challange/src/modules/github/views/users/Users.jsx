import React from 'react';
import './Users.css'
import UserGrid from './components/grid/UserGrid';

import brand from '../../../../assets/img/brand.png'
import Modal from './components/modal/Modal';

export default function Users() {

  return (
    <section>
      <a href="https://shawandpartners.com/" target="_blank">
        <img src={brand} className="w-25 float-right" alt="shaw and partners" />
      </a>

      <h1>User List</h1>

      <p>
        Below is the list of github users, click on the column you want to see more details.
      </p>

      <UserGrid />
      <Modal /> 
    </section>
  );
}
