import React from 'react';
import { Router, Route, Switch, Redirect } from "react-router-dom"
import {Provider} from 'react-redux'

import store from './store'
import { createBrowserHistory } from "history"

import DefaultLayout from './modules/github/layouts/DefaultLayout';

const history = createBrowserHistory()

const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route path="/github" render={props => <DefaultLayout {...props}  />} />
        <Redirect from="/" to="/github/users" />
      </Switch> 
    </Router>
  </Provider>
)

export default App;
