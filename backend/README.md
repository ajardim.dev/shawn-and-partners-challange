![](https://shawandpartners.com/wp-content/uploads/2017/02/LOGO_FINAL_SP.png)
## Arthur Jardim - +55 (51) 9 8555-0717

### Why Adonis Js

For this application, I chose AdonisJS because it is very productive and simple and is a framework that brings an architectural opinion and helps a lot when you have programmers with different skill levels and working remotely.

This comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Get Started

```bash
adonis migration:run
adonis serve --dev
```
## Test

```bash
adonis test
```