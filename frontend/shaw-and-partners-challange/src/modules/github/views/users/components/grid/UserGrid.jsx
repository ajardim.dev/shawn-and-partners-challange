import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import { UsersService } from '../../services/UserService'
import {Types} from '../../../../../../store/ducks/users'
import InfiniteScroll from 'react-infinite-scroll-component';

import './UserGrid.css'

export default function UserGrid() {
    const [since, setSince] = useState(0)
    const dispatch = useDispatch()

    useEffect(() => {
        getAll()
    }, [since])
    
    async function getAll() {
        const service = new UsersService()
        const users = await service.getAll({since: since})

        dispatch({ type: Types.ADD_USERS, users })
    }

    function getMore() {
        const last = users[users.length-1]
        setSince(last.id)
    }

    async function openModal(user) {
        if(user === null)
            return
            
        const service = new UsersService()
        const newUser = await service.get(user)
        dispatch({ type: Types.SHOW_USER, user: newUser})

        const repos = await service.getRepos(user)
        dispatch({ type: Types.USER_REPOS, userRepos: repos})
    }

    const users = useSelector(state => state.users.users)

    return <InfiniteScroll
            style={{ height: "100%", overflow: "" }}
            dataLength={users.length}
            next={getMore}
            hasMore={true}
            threshold={350}
            endMessage="">
        <table className="user-grid table mt-3">
            <thead>
                <tr>
                    <th className="id">ID</th>
                    <th className="name">User</th>
                </tr>
            </thead>
            <tbody>
               {users.map((user, i) => <tr onClick={() => openModal(user)} key={i}>
                    <td>
                        <span>
                            {user.id}
                        </span>
                    </td>
                    <td>
                        <div className="float-left mr-2">
                            <img src={user.avatar_url} className="rounded-circle avatar"></img>
                        </div>
                        <span className="p-3">
                            <strong>{user.login}</strong>
                        </span>
                    </td>
                </tr>)} 
            </tbody>
        </table>
    </InfiniteScroll>
}
