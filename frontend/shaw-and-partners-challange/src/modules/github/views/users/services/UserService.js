const queryString = require('query-string')

export class UsersService {
    async getAll(request) {
        const baseUrl = process.env.REACT_APP_API

        const results = await fetch(`${baseUrl}/users?${queryString.stringify(request)}`, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })

        const users = await results.json()

        return users
    }

    async get(request) {
        const baseUrl = process.env.REACT_APP_API

        const results = await fetch(`${baseUrl}/users/${request.login}/details`, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })

        const user = await results.json()

        return user
    }

    async getRepos(request) {
        const baseUrl = process.env.REACT_APP_API

        const results = await fetch(`${baseUrl}/users/${request.login}/repos`, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })

        const user = await results.json()

        return user
    }
}
