import React from 'react';
import {NavLink} from 'react-router-dom'

export default function Sidebar(props) {
    const {routes} = props

    return <aside className="col-12 col-md-1 p-0 bg-dark flex-shrink-1">
        <nav className="navbar navbar-expand navbar-dark bg-dark flex-md-column flex-row align-items-start w-100 my-4 p-0">
            <div className="collapse navbar-collapse w-100">
                <ul className="flex-md-column flex-row navbar-nav w-100 justify-content-between">
                    {routes.map((route,i) => <li key={i} className="nav-item text-center item-menu w-100">
                        <NavLink id="githubUsers" to={`${route.module}${route.path}`}>
                            <i className={route.icon}></i><br />
                            <span>{route.label}</span>
                        </NavLink>
                    </li>)}
                </ul>
            </div>
        </nav>
    </aside>
}
