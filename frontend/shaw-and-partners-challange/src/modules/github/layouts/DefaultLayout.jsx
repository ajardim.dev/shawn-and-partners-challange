import React from 'react';

import  './DefaultLayout.css'
import Sidebar from './components/Sidebar';

import routes from '../routes.github'
import Main from './components/Main';

export default function DefaultLayout() {
  return <div className="container-fluid">
      <div className="row min-vh-100 flex-column flex-md-row">
          <Sidebar routes={routes} />
          <Main routes={routes}/>
      </div>
  </div>
}
