const { test, trait } = use('Test/Suite')('Users From Github')

trait('Test/ApiClient')

test('Should get users from github as success', async ({ client, assert }) => {
    const response = await client
        .get('users?since=5')
        .end()
    response.assertStatus(200)
})

test('Should get users from github paging by since parameter as success', async ({ client, assert }) => {
    const response = await client
        .get('users?since=5')
        .end()
    response.assertStatus(200)
    
    const data = JSON.parse(response.text)
    if (data.length > 0) {
        assert.isTrue(data[0].id > 5)
    }
})

test('Should get details of user from github api as success', async ({ client, assert }) => {
    const response = await client
        .get('users/shawandpartners/details')
        .end()
    response.assertStatus(200)
})

test('Should get details of shawandpartners from github api as success', async ({ client, assert }) => {
    const response = await client
        .get('users/shawandpartners/details')
        .end()
    response.assertStatus(200)

    const data = JSON.parse(response.text)
    assert.equal(data.login, 'shawandpartners')

})

test('Should get shawandpartners created at 2012/12/05 date as success', async ({ client, assert }) => {
    const response = await client
        .get('users/shawandpartners/details')
        .end()
    response.assertStatus(200)

    const data = JSON.parse(response.text)

    assert.equal(data.created_at, '2012-12-05T23:00:54Z')
})

test('Should get reduxjs repos  as success', async ({ client, assert }) => {
    const response = await client
        .get('users/reduxjs/repos')
        .end()
    response.assertStatus(200)
})

test('Should get react-redux repo as success', async ({ client, assert }) => {
    const response = await client
        .get('users/reduxjs/repos')
        .end()
    response.assertStatus(200)

    const data = JSON.parse(response.text)

    const reactRedux = data.find(repo => repo.name === 'react-redux')
    assert.isTrue(!!reactRedux)

})

