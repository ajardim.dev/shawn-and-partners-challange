import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import {Types} from '../../../../../../store/ducks/users'
import { UsersService } from '../../services/UserService'
import {  SwipeableDrawer, TextField } from '@material-ui/core';


import moment from "moment"

import './Modal.css'

export default function Modal(props) {
    const dispatch = useDispatch()
    const user = useSelector(state => state.users.user)
    const repos = useSelector(state => state.users.userRepos)

    if(user === null)
        return null
        
    return <SwipeableDrawer 
			open={user !== null}
			anchor="right"
			onClose={(e) => {
				dispatch({type: Types.SHOW_USER, user: null})
			}} 
            onOpen={() => console.log('openning')}>
		<div className='side-modal' >
            <div className='title'>
                <a className="close-side-modal" onClick={(e) => { dispatch({type: Types.SHOW_USER, user: null})}} >
                    <i className="fal fa-long-arrow-left"></i>
                </a><br />
                <h2 className="mx-auto text-center w-25"><img src={user.avatar_url} className="rounded-circle w-100 avatar-md"></img><br /><br />{user.login}</h2>
            </div> 

            <div className='content' >
                <div className="p-2 d-flex mb-3" >
                    <TextField
                        label={<span>ID</span>}
                        InputLabelProps={{
                            shrink: true
                        }}
                        id="id"
                        className="form-control"
                        value={user.id}
                         />
                </div>

                <div className="p-2 d-flex mb-3" >
                    <TextField
                        label={<span>Name</span>}
                        InputLabelProps={{
                            shrink: true
                        }}
                        id="name"
                        className="form-control"
                        value={user.name} 
                        />
                </div>

                <div className="p-2 d-flex mb-3" >
                    <TextField
                        label={<span>Profile URL</span>}
                        InputLabelProps={{
                            shrink: true
                        }}
                        id="id"
                        className="form-control"
                        value={user.html_url} 
                        />
                </div>

                <div className="p-2 d-flex mb-3" >
                    <TextField
                        label={<span>Created At</span>}
                        InputLabelProps={{
                            shrink: true
                        }}
                        id="id"
                        className="form-control"
                        value={moment(user.created_at).format("LLLL")} 
                        />
                </div>
                <div className="p-2 d-flex mb-3 mt-5" >
                    <h5>{user.name} <strong>Repos</strong></h5>
                </div>

                <div className="p-2 d-flex mb-3 " >
                    <table className="table user-grid">
                        <thead>
                            <tr>
                                <th className="id">ID</th>
                                <th className="name">Repo</th>
                            </tr>
                        </thead>
                        <tbody>
                            {repos.length === 0 
                                ?  <tr>
                                    <td colSpan="2" className="text-center">
                                        No repo to display
                                    </td>
                                </tr>
                                : repos.map((repo, i) => <tr key={i} onClick={() => window.open(repo.html_url, '_blank')}>
                                    <td>
                                        <span>
                                            {repo.id}
                                        </span>
                                    </td>
                                    <td>
                                        <span>
                                            {repo.name}
                                            <br />
                                            <a target="_blank" className="link-repo" href={repo.html_url} >{repo.html_url}</a>
                                        </span>
                                        
                                    </td>
                                </tr>)}
                        </tbody>
                    </table>
                </div>


            </div>
		</div>
	  </SwipeableDrawer>
}
